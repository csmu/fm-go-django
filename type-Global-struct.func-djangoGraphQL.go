package main

import (
	"bytes"
	"crypto/tls"
	"io/ioutil"
	"net/http"
	"time"
)

func (global *Global) djangoGraphQL(client *user, data []byte) ([]byte, error) {

	var result []byte
	var err error

	var transportHTTP *http.Transport
	var requestHTTP *http.Request
	var clientHTTP *http.Client
	var responseHTTP *http.Response
	var crsfTokenCookie http.Cookie
	var sessionIDCookie http.Cookie
	var expiresTime time.Time

	transportHTTP = &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: false},
	}

	requestHTTP, err = http.NewRequest("POST", global.Django.GraphQLPath, bytes.NewBuffer(data))

	if err != nil {

		result = []byte(err.Error())

	} else {

		requestHTTP.Header.Set("Content-Type", "application/json")
		requestHTTP.Header.Set("X-CSRFToken", client.CSRFToken)

		expiresTime = time.Now().Add(10 * time.Minute)
		sessionIDCookie = http.Cookie{Name: "sessionid", Value: client.SessionID, Path: "/", Expires: expiresTime, MaxAge: 90000}
		crsfTokenCookie = http.Cookie{Name: "csrftoken", Value: client.CSRFToken, Path: "/", Expires: expiresTime, MaxAge: 90000}

		requestHTTP.AddCookie(&sessionIDCookie)
		requestHTTP.AddCookie(&crsfTokenCookie)

		clientHTTP = &http.Client{Transport: transportHTTP}

		responseHTTP, err = clientHTTP.Do(requestHTTP)

		if err != nil {
			result = []byte(err.Error())
		} else {
			result, _ = ioutil.ReadAll(responseHTTP.Body)
		}

	}

	return result, err
}
