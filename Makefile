APP=fm-go-django

build: clean
	go build -o ${APP} *.go
	
race:
	go -race main.go
	
clean:
	go clean