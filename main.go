package main

import (
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

var (
	global Global
)

func main() {

	var err error
	var environmentPath string
	var dir string
	var router *mux.Router
	var versionPath string

	versionPath = filepath.Join(dir, "version", "version.json")
	global.Version, _ = loadVersionFrom(versionPath)

	dir, err = filepath.Abs(filepath.Dir(os.Args[0]))
	fatal(err)

	environmentPath = filepath.Join(dir, ".env")
	fmt.Println(environmentPath)

	err = godotenv.Load(environmentPath)
	fatal(err)
	global.mapEnvironmentVariables()

	global.loadRSAKeys(dir)

	router = mux.NewRouter()

	router.HandleFunc("/", global.versionHandler).Methods("GET", "OPTIONS")

	router.HandleFunc("/graphql", global.graphQLHandler).Methods("GET", "POST", "OPTIONS")
	router.HandleFunc("/graphql/", global.graphQLHandler).Methods("GET", "POST", "OPTIONS")

	router.HandleFunc("/request", global.requestHandler).Methods("GET", "OPTIONS")
	router.HandleFunc("/request/", global.requestHandler).Methods("GET", "OPTIONS")

	router.HandleFunc("/send/sms/motr/suburban", sendSMSMotrSuburbanHandler).Methods("GET", "OPTIONS")

	var fullchainPath = filepath.Join(dir, "fullchain.pem")
	var privkeyPath = filepath.Join(dir, "privkey.pem")

	if _, err := os.Stat(fullchainPath); os.IsNotExist(err) {
		fmt.Printf("Starting server listening at http://%s\n", global.Listen)
		err = http.ListenAndServe(global.Listen, handlers.CombinedLoggingHandler(os.Stdout, router))
		if err != nil {
			fmt.Println("Unexpected error", err)
		}
	} else {
		fmt.Printf("Starting server listening at https://%s\n", global.Listen)
		err = http.ListenAndServeTLS(global.Listen, fullchainPath, privkeyPath, handlers.CombinedLoggingHandler(os.Stdout, router))
		if err != nil {
			fmt.Println("Unexpected error", err)
		}
	}

}
