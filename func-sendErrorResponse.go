package main

import (
	"encoding/json"
	"net/http"
)

func sendErrorResponse(w http.ResponseWriter, err string, status int) {
	var errorMessage ErrorMessage
	errorMessage = ErrorMessage{}
	errorMessage.Error = err
	errorMessage.Status = status
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(errorMessage)
}
