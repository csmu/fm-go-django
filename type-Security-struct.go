package main

import "crypto/rsa"

// Security is used to create JSON Web Tokens.
type Security struct {
	PrivateKeyPath string `json:"privateKeyPath"`
	PublicKeyPath  string `json:"publicKeyPath"`
	VerifyKey      *rsa.PublicKey
	SignKey        *rsa.PrivateKey
}
