package main

// ErrorMessage is used to pass back json error messages.
type ErrorMessage struct {
	Error  string `json:"error"`
	Status int    `json:"status"`
}
