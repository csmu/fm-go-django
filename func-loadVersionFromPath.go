package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

func loadVersionFrom(path string) (*Version, error) {

	var err error
	var byteSlice []byte
	var file *os.File
	var result *Version

	file, err = os.Open(path)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		byteSlice, err = ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			err = json.Unmarshal(byteSlice, &result)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	}

	return result, err
}
