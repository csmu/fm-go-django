package main

// ParametersErrors Used to send back pre condition parameters errors.
type ParametersErrors struct {
	Status int      `json:"status"`
	Error  []string `json:"errors"`
}
