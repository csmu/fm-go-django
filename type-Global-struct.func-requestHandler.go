package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (global *Global) requestHandler(w http.ResponseWriter, r *http.Request) {

	var err error
	var ok bool
	var keys []string
	var uuid string
	var query *graphQLQuery
	var data []byte
	var body []byte

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		fmt.Println("origin", origin)
	}
	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		return
	}

	if r.Method == "GET" {
		w.Header().Set("Content-Type", "application/json")

		if global.Django.Bot == nil {
			global.Django.Bot, err = global.Django.Login()
		}
		if err == nil {

			keys, ok = r.URL.Query()["uuid"]
			if !ok {
				sendErrorResponse(w, "uuid is a required parameter", http.StatusBadRequest)
			} else {
				uuid = keys[0]
				var request string
				request = fmt.Sprintf(`query {
	request(uuid:"%s"){
		name,
		parameters,
		processedCount,
		modificationTimestamp,
		results
	}
}`, uuid)
				query = &graphQLQuery{}
				query.Query = request
				data, err = json.Marshal(query)
				fmt.Println(string(data))
				if err != nil {
					sendErrorResponse(w, err.Error(), http.StatusBadRequest)
				} else {
					var client *user
					client = &user{}
					client.CSRFToken = global.Django.Bot.Cookie("csrftoken").Value
					client.SessionID = global.Django.Bot.Cookie("sessionid").Value
					body, err = global.djangoGraphQL(client, data)
					if err != nil {
						sendErrorResponse(w, err.Error(), http.StatusBadRequest)
					} else {
						w.WriteHeader(http.StatusOK)
						fmt.Fprintf(w, "%s\ngit status", body)
					}
				}
			}
		} else {
			sendErrorResponse(w, err.Error(), http.StatusInternalServerError)
		}
	}
}
