package main

import (
	"encoding/xml"
	"fmt"
	"net/http"
)

func sendSMSMotrSuburbanHandler(w http.ResponseWriter, r *http.Request) {

	var err error
	var ok bool
	var keys []string
	var motr *MotrSuburban
	var parametersErrors *ParametersErrors
	var bytes []byte

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		fmt.Println("origin", origin)
	}
	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		return
	}

	if r.Method == "GET" {
		w.Header().Set("Content-Type", "text/xml")

		motr = &MotrSuburban{}
		parametersErrors = &ParametersErrors{}
		parametersErrors.Status = http.StatusBadRequest

		fmt.Println(r.URL.Query())
		fmt.Println(r.URL.RawPath)

		keys, ok = r.URL.Query()["username"]
		if !ok {
			parametersErrors.Error = append(parametersErrors.Error, "username is a required parameter")
		} else {
			motr.Username = keys[0]
		}
		keys, ok = r.URL.Query()["password"]
		if !ok {
			parametersErrors.Error = append(parametersErrors.Error, "password is a required parameter")
		} else {
			motr.Password = keys[0]
		}
		keys, ok = r.URL.Query()["mobile"]
		if !ok {
			parametersErrors.Error = append(parametersErrors.Error, "mobile is a required parameter")
		} else {
			motr.Mobile = keys[0]
		}
		keys, ok = r.URL.Query()["message"]
		if !ok {
			parametersErrors.Error = append(parametersErrors.Error, "message is a required parameter")
		} else {
			motr.Message = keys[0]
		}
		if len(parametersErrors.Error) > 0 {
			bytes, err = xml.Marshal(&parametersErrors)
			if err != nil {
				sendErrorResponse(w, err.Error(), http.StatusInternalServerError)
			} else {
				fmt.Fprintf(w, "%s", string(bytes))
			}
		} else {
			err = motr.sendMessage()
			if err != nil {
				sendErrorResponse(w, err.Error(), http.StatusInternalServerError)
			} else {
				fmt.Fprintf(w, "%s", string(motr.Results))
			}
		}

	} else {
		sendErrorResponse(w, err.Error(), http.StatusInternalServerError)
	}
}
