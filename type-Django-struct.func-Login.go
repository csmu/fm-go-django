package main

import (
	"errors"
	"fmt"

	"github.com/hemantasapkota/djangobot"
)

// Login to a Django service.
func (django *Django) Login() (*djangobot.Bot, error) {

	var bot *djangobot.Bot
	var csrfToken string
	var err error
	var connect string

	bot = djangobot.With(django.LoginPath)

	if bot == nil {
		err = errors.New("Unexpected error: *djangobot.Bot is nil")
	} else {
		if django.Port == "" {
			connect = django.Host
		} else {
			connect = fmt.Sprintf("%s:%s", django.Host, django.Port)
		}
		bot.ForHost(connect)
		bot.SetUsername(django.Username)
		bot.SetPassword(django.Password)
		bot.LoadCookies()

		if bot.Error == nil {

			csrfToken = bot.Cookie("csrftoken").Value

			_, _ = bot.Set("next", django.GraphQLPath).
				X("csrfmiddlewaretoken", csrfToken).
				X("username", bot.Username).
				X("password", bot.Password).
				Login()

		} else {
			err = bot.Error
		}
	}

	return bot, err
}
