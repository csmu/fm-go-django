package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func (global *Global) graphQLHandler(w http.ResponseWriter, r *http.Request) {

	var body []byte
	var client *user
	var err error
	var data []byte
	var query *graphQLQuery

	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		fmt.Println("origin", origin)
	}
	// Stop here if its Preflighted OPTIONS request
	if r.Method == "OPTIONS" {
		return
	}

	w.Header().Set("Content-Type", "application/json")
	authorised, _, _, client := global.verifyAuthorizationHeaderClient(w, r)

	if authorised {

		if r.Method == "GET" {
			query = &graphQLQuery{}
			query.Query = r.URL.Query()["query"][0]
			data, err = json.Marshal(query)
			if err != nil {
				sendErrorResponse(w, err.Error(), http.StatusBadRequest)
			} else {
				body, err = global.djangoGraphQL(client, data)
				if err != nil {
					sendErrorResponse(w, err.Error(), http.StatusBadRequest)
				} else {
					w.WriteHeader(http.StatusOK)
					fmt.Fprintf(w, "%s", body)
				}
			}
		}

		if r.Method == "POST" {

			body, err = ioutil.ReadAll(r.Body)
			if err != nil {
				sendErrorResponse(w, err.Error(), http.StatusBadRequest)
			} else {

				if err != nil {
					sendErrorResponse(w, err.Error(), http.StatusBadRequest)
				} else {

					err = json.Unmarshal(bytes.ReplaceAll(bytes.ReplaceAll(body, []byte("\n"), nil), []byte("\t"), nil), &query)
					if err != nil {
						sendErrorResponse(w, err.Error(), http.StatusBadRequest)
					} else {

						data, err = json.Marshal(query)

						if err != nil {

							sendErrorResponse(w, err.Error(), http.StatusBadRequest)

						} else {

							body, err = global.djangoGraphQL(client, data)
							if err != nil {
								sendErrorResponse(w, err.Error(), http.StatusBadRequest)
							} else {
								w.WriteHeader(http.StatusOK)
								fmt.Fprintf(w, "%s", body)
							}

						}
					}
				}
			}
		}
	}
}
