package main

import "github.com/hemantasapkota/djangobot"

// Django is used to talk to a django server.
type Django struct {
	Host        string `json:"host"`
	Port        string `json:"port"`
	LoginPath   string `json:"loginPath"`
	GraphQLPath string `json:"graphQLPath"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Bot         *djangobot.Bot
}
