package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func (motr *MotrSuburban) sendMessage() error {
	var err error
	var httpMethod = "POST"
	var soapAction string
	var payload []byte
	var request *http.Request
	var transport *http.Transport
	var client *http.Client
	var response *http.Response
	var message string

	if motr.Link == "" {
		motr.Link = "http://motr.com.au/server.php?wsdl"
	}

	message, err = url.QueryUnescape(motr.Message)
	if err != nil {
		message = motr.Message
	}
	soapAction = "urn:suburbanwebservice"
	payload = []byte(fmt.Sprintf(strings.TrimSpace(`
	<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
    <Body>
        <sendMessage xmlns="urn:suburbanwebservice">
            <Username>%s</Username>
            <Password>%s</Password>
            <Mobile>%s</Mobile>
            <Message>%s</Message>
        </sendMessage>
    </Body>
</Envelope>`), motr.Username, motr.Password, motr.Mobile, message))
	request, err = http.NewRequest(httpMethod, motr.Link, bytes.NewReader(payload))
	if err == nil {
		request.Header.Set("Content-type", "text/xml")
		request.Header.Set("SOAPAction", soapAction)
		transport = &http.Transport{}
		client = &http.Client{Transport: transport}
		response, err = client.Do(request)
		if err == nil {
			if response.Body != nil {
				defer response.Body.Close()
				motr.Results, err = ioutil.ReadAll(response.Body)
			}
		}
	}
	return err
}
