package main

// Global is for global variables
type Global struct {
	Host     string   `json:"host"`
	Port     string   `json:"port"`
	Listen   string   `json:"listen"`
	Django   Django   `json:"django"`
	Security Security `json:"security"`
	Version  *Version `json:"version"`
}
