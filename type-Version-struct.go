package main

import "time"

// Version is the current version of the application.
type Version struct {
	Prefix *string   `json:"prefix"`
	Major  *int      `json:"major"`
	Minor  *int      `json:"minor"`
	Bug    time.Time `json:"bug"`
	Text   string    `json:"text"`
}
