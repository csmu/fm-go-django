package main

type user struct {
	ID             int    `json:"id"`
	Name           string `json:"name"`
	Email          string `json:"email"`
	MD5            string `json:"md5"`
	IsSuperUser    bool   `json:"isSuperUser"`
	DepotID        int    `json:"depotID"`
	DepotName      string `json:"depotName"`
	WebChannelMain string `json:"webChannelMain"`
	Host           string `json:"host"`
	Database       string `json:"database"`
	IsStaff        bool   `json:"isStaff"`
	IsDriver       bool   `json:"isDriver"`
	SessionID      string `json:"sessionID"`
	CSRFToken      string `json:"csrfToken"`
}
