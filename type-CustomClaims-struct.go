package main

import "github.com/dgrijalva/jwt-go"

// CustomClaims is used to create JSON Web Tokens.
type CustomClaims struct {
	*jwt.StandardClaims
	TokenType string
	user
}
