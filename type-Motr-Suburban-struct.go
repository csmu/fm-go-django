package main

// MotrSuburban is used to send SMS messages to motr.com.au
type MotrSuburban struct {
	Link     string `json:"link"`
	Username string `json:"username"`
	Password string `json:"password"`
	Mobile   string `json:"mobile"`
	Message  string `json:"message"`
	Results  []byte `json:"results"`
}
