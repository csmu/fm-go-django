package main

import (
	"fmt"
	"os"
	"strings"
)

func (global *Global) mapEnvironmentVariables() {

	for _, e := range os.Environ() {
		pair := strings.Split(e, "=")
		switch pair[0] {
		case "HOST":
			global.Host = pair[1]
		case "PORT":
			global.Port = pair[1]
		case "DJANGO_HOST":
			global.Django.Host = pair[1]
		case "DJANGO_PORT":
			global.Django.Port = pair[1]
		case "DJANGO_LOGIN_PATH":
			global.Django.LoginPath = pair[1]
		case "DJANGO_GRAPHQL_PATH":
			global.Django.GraphQLPath = pair[1]
		case "DJANGO_USERNAME":
			global.Django.Username = pair[1]
		case "DJANGO_PASSWORD":
			global.Django.Password = pair[1]
		case "PUBLIC_KEY_PATH":
			global.Security.PublicKeyPath = pair[1]
		case "PRIVATE_KEY_PATH":
			global.Security.PrivateKeyPath = pair[1]
		}
	}
	global.Listen = fmt.Sprintf("%s:%s", global.Host, global.Port)

}
