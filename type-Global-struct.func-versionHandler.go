package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (global *Global) versionHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == "OPTIONS" {
		fmt.Println("Origin", r.Header.Get("Origin"))
		if origin := r.Header.Get("Origin"); origin != "" {
			w.Header().Set("Access-Control-Allow-Origin", origin)
		}
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		w.WriteHeader(http.StatusOK)
		return
	}

	if r.Method == "GET" {
		w.WriteHeader(http.StatusOK)
		var byteSlice []byte
		w.Header().Set("Content-Type", "application/json")
		byteSlice, _ = json.MarshalIndent(global.Version, "", "    ")
		fmt.Fprintf(w, "%s\n", string(byteSlice))
	}

}
