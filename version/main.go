package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"
)

type version struct {
	Prefix *string   `json:"prefix"`
	Major  *int      `json:"major"`
	Minor  *int      `json:"minor"`
	Bug    time.Time `json:"bug"`
	Text   string    `json:"text"`
}

type fileInputOutput struct {
	Path   *string `json:"path"`
	Exists bool    `json:"exists"`
}

type inputOutput struct {
	Input  fileInputOutput `json:"input"`
	Output fileInputOutput `json:"output"`
}

func pathExists(path *string) bool {
	var result = false
	var err error
	_, err = os.Stat(*path)
	if err == nil {
		result = true
	}
	return result
}

func (version *version) mapPrevious(previous *version) {
	if version != nil {
		if previous != nil {

			var prefix = *version.Prefix
			var major = *version.Major
			var minor = *version.Minor
			var location *time.Location

			location, _ = time.LoadLocation("UTC")

			if prefix == "" {
				version.Prefix = previous.Prefix
			}
			if major == -1 {
				version.Major = previous.Major
			}
			if minor == -1 {
				minor = *previous.Minor
				minor++
				version.Minor = &minor
			}
			version.Text = fmt.Sprintf("%s-v%d.%d.%s", *version.Prefix, *version.Major, *version.Minor, version.Bug.In(location).Format("20060102-150405-UTC"))
		}
	}
}

func loadFrom(path string) (*version, error) {

	var err error
	var byteSlice []byte
	var file *os.File
	var result *version

	file, err = os.Open(path)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		byteSlice, err = ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err.Error())
		} else {
			err = json.Unmarshal(byteSlice, &result)
			if err != nil {
				fmt.Println(err.Error())
			}
		}
	}

	return result, err
}

func main() {

	var byteSlice []byte
	var directoryPath string
	var err error
	var file *os.File
	var inputOutput = &inputOutput{}
	var previousVersion = &version{}
	var versionPath string
	var version = &version{}

	directoryPath, err = filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	versionPath = filepath.Join(directoryPath, "version.json")

	inputOutput.Input.Path = flag.String("input", "", "A path to a valid json version input file. Default is version.json")
	inputOutput.Output.Path = flag.String("output", "", "A path to a valid json version output file. Default is version.json")

	if *inputOutput.Input.Path == "" {
		inputOutput.Input.Path = &versionPath
	}
	if *inputOutput.Output.Path == "" {
		inputOutput.Output.Path = &versionPath
	}
	inputOutput.Input.Exists = pathExists(inputOutput.Input.Path)
	inputOutput.Output.Exists = pathExists(inputOutput.Output.Path)

	version.Prefix = flag.String("prefix", "", "The prefix you'd like for the project")
	version.Major = flag.Int("major", -1, "The major version of the project")
	version.Minor = flag.Int("minor", -1, "The minor version of the project")
	version.Bug = time.Now()

	flag.Parse()

	byteSlice, err = json.Marshal(inputOutput)
	if err != nil {
		log.Fatal(err)
	} else {
		if inputOutput.Input.Exists {
			previousVersion, err = loadFrom(*inputOutput.Input.Path)
			if err != nil {
				fmt.Println(err.Error())
			} else {
				version.mapPrevious(previousVersion)
				byteSlice, err = json.MarshalIndent(version, "", "    ")
				fmt.Printf("%s\n", string(byteSlice))
				file, err = os.Create(*inputOutput.Output.Path)
				if err != nil {
					fmt.Println(err.Error())
				} else {
					defer file.Close()
					file.WriteString(string(byteSlice))
					file.Sync()
				}
			}
		}
	}
}
