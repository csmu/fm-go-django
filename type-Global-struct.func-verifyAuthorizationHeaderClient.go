package main

import (
	"fmt"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
)

func (global *Global) verifyAuthorizationHeaderClient(w http.ResponseWriter, r *http.Request) (bool, int, bool, *user) {

	var authenticated = false
	var id = 0
	var isSuperUser = false
	var client *user

	token, err := request.ParseFromRequestWithClaims(r, request.OAuth2Extractor, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		// since we only use the one private key to sign the tokens,
		// we also only use its public counter part to verify
		return global.Security.VerifyKey, nil
	})

	// If the token is missing or invalid, return error
	if err != nil {
		var text = fmt.Sprintf("Invalid Depot Maestro JSON Web Token: %v", err)
		fmt.Println(text)
		sendErrorResponse(w, text, http.StatusUnauthorized)
	} else {
		// Token is valid
		client = &user{
			token.Claims.(*CustomClaims).ID,
			token.Claims.(*CustomClaims).Name,
			token.Claims.(*CustomClaims).Email,
			token.Claims.(*CustomClaims).MD5,
			token.Claims.(*CustomClaims).IsSuperUser,
			token.Claims.(*CustomClaims).DepotID,
			token.Claims.(*CustomClaims).DepotName,
			token.Claims.(*CustomClaims).WebChannelMain,
			token.Claims.(*CustomClaims).Host,
			token.Claims.(*CustomClaims).Database,
			token.Claims.(*CustomClaims).IsStaff,
			token.Claims.(*CustomClaims).IsDriver,
			token.Claims.(*CustomClaims).SessionID,
			token.Claims.(*CustomClaims).CSRFToken,
		}
		//fmt.Println(w, "Welcome,", token.Claims.(*CustomClaims).Name, token.Claims.(*CustomClaims).ID, token.Claims.(*CustomClaims).IsSuperUser)
		authenticated = true
		id = token.Claims.(*CustomClaims).ID
		isSuperUser = token.Claims.(*CustomClaims).IsSuperUser
	}

	return authenticated, id, isSuperUser, client
}
