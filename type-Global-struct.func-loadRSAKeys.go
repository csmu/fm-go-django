package main

import (
	"io/ioutil"
	"path/filepath"

	"github.com/dgrijalva/jwt-go"
)

func (global *Global) loadRSAKeys(dir string) {
	var err error
	var verifyBytes []byte
	var signBytes []byte
	signBytes, err = ioutil.ReadFile(filepath.Join(dir, global.Security.PrivateKeyPath))
	fatal(err)
	global.Security.SignKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	fatal(err)
	verifyBytes, err = ioutil.ReadFile(filepath.Join(dir, global.Security.PublicKeyPath))
	fatal(err)
	global.Security.VerifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	fatal(err)
}
